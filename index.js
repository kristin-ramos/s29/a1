const express=require('express');
const app=express();
const PORT=4005;

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// app.get("/home",(req,res)=>{
//     console.log("Welcome to the home page")
// })

// app.get("/home",(req,res)=>{
//     res.status(200).send(`Welcome to the home page`)
// })

const user=[
    {
        userName: "Kris"
    },
    {
        userName: "Grace"
    }
];
// app.get("/users", ()=>{
//     console.log(user)
// })

// app.get("/users", (req, res)=>{
//     res.status(200).send(user)
// })

app.delete("/delete-user",(req, res)=>{
    const{userName}=req.body
    res.status(200).send(`User ${userName} has been deleted`)
})

app.listen(PORT, ()=> console.log(`Server connected to ${PORT}`))